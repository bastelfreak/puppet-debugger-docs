<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [About Me](#about-me)
- [Puppet Debugger Workshop](#puppet-debugger-workshop)
  - [Summary](#summary)
  - [Quick Links](#quick-links)
  - [FAQs](#faqs)
    - [What is a Debugger?](#what-is-a-debugger)
    - [Why do we need a debugger for Puppet code?](#why-do-we-need-a-debugger-for-puppet-code)
    - [Is Puppet Debugger a unit test replacement?](#is-puppet-debugger-a-unit-test-replacement)
    - [What does Puppet Debugger replace?](#what-does-puppet-debugger-replace)
    - [What makes Puppet Debugger unique?](#what-makes-puppet-debugger-unique)
    - [Where you can run Puppet Debugger?](#where-you-can-run-puppet-debugger)
    - [Which Puppet versions does the debugger support?](#which-puppet-versions-does-the-debugger-support)
    - [What kinds of things can a normal debugger do?](#what-kinds-of-things-can-a-normal-debugger-do)
    - [What can Puppet Debugger do?](#what-can-puppet-debugger-do)
    - [What does Puppet Debugger not do?](#what-does-puppet-debugger-not-do)
    - [Where can Puppet Debugger be used?](#where-can-puppet-debugger-be-used)
    - [Why is Puppet Debugger better than the normal `puppet apply`?](#why-is-puppet-debugger-better-than-the-normal-puppet-apply)
    - [Things Puppet Debugger needs internally to evaluate code?](#things-puppet-debugger-needs-internally-to-evaluate-code)
  - [Exercises](#exercises)
    - [1. Installation](#1-installation)
      - [Docker method (easiest)](#docker-method-easiest)
      - [Installing alongside the Puppet Agent's Ruby](#installing-alongside-the-puppet-agents-ruby)
      - [Installing on the PDK](#installing-on-the-pdk)
      - [Installing in user-supplied Ruby](#installing-in-user-supplied-ruby)
    - [1b. Verify the installation works](#1b-verify-the-installation-works)
    - [2. Installing Modules](#2-installing-modules)
    - [3. Running with a manifest](#3-running-with-a-manifest)
      - [Sharable links](#sharable-links)
    - [4. Listing and executing functions](#4-listing-and-executing-functions)
    - [5. Listing resources and evaluated parameters](#5-listing-resources-and-evaluated-parameters)
    - [6. Listing facts and variables](#6-listing-facts-and-variables)
    - [7. Prototyping datatypes](#7-prototyping-datatypes)
      - [Datatype comparisons](#datatype-comparisons)
      - [Datatype composition](#datatype-composition)
    - [8. Hiera debugging](#8-hiera-debugging)
    - [9. Turning on puppet debug messages.](#9-turning-on-puppet-debug-messages)
    - [10. Mocking custom facts or using prebuilt facts](#10-mocking-custom-facts-or-using-prebuilt-facts)
    - [11. Using a breakpoint](#11-using-a-breakpoint)
    - [12. Breaking into your code](#12-breaking-into-your-code)
      - [Play Time](#play-time)
    - [Summary](#summary-1)
  - [How can you help?](#how-can-you-help)
      - [Documentation](#documentation)
      - [Code cleanup](#code-cleanup)
      - [New plugins](#new-plugins)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# About Me
Hi!  My name is Corey Osman and I have been in the Puppet community for a decade writing devops tools and Puppet code.  You may have seen me as logicminds or nwops
on the internet.  I work as a *Puppet Service Delivery Partner* at NWOPS, LLC where I provide unique software, training and consulting.  

My passion is programming and crafting hacks for "impossible situations."  I am also a big crypto miner and crafted a [tool](https://crossbelt.blockops.party) built around Puppet tooling to manage my fleet of rigs.

You can reach me at: 

  * `nwops` (Puppet community slack)
  * `@nw_ops` on twitter 
  * `@opselite` on telegram.
  * `@logicminds` on github
  * `@coreynwops` on gitlab

# Puppet Debugger Workshop

## Summary
Puppet Debugger is a CLI tool built from the ground up to allow interactive debugging of puppet code in real time.  This tool is similar to Ruby's Pry Debugger but specifically built for Puppet code.  This workshop will detail the basic functionality as well as some advanced commands like using breakpoints, scope inspection, Hiera debugging, live variable interpolation, function interaction, datatype prototyping and more.   Get ready to dig into your Puppet code like never before.


## Quick Links
- [Official Puppet Debugger website](http://puppet-debugger.com)
- [Puppet Debugger blog](http://logicminds.github.io/categories/debugger/)
- [Github project page](https://github.com/nwops/puppet-debugger/blob/master/README.md)
- [Live demo](http://demo.puppet-debugger.com)
- [Official NWOPS site](https://www.nwops.io)
- [Puppet podcast about Puppet Debugger](https://puppet.com/podcasts/community-spotlight-puppet-debugger)

## FAQs
Before we start, please read the following questions and answers as a primer to discover why I built this tool and how it can help solve problems. 

### What is a Debugger?
A debugger is a tool present in almost all programming languages that allows you to see how the compiler evaluates your code as if you were standing inside the compiler with a poking stick.

Many debuggers and REPLs can be found [online](https://repl.it/languages)

### Why do we need a debugger for Puppet code?
Puppet code can become quite complex when using some of the advanced functionality found inside the Puppet language.  Additionally, Puppet code is used across multiple devops tools like Bolt so it makes sense to have a way to debug Puppet code when a unit test is not possible or feasible.

Below are just a few reasons why you would use a debugger: 

  * Use with Bolt for plans and tasks
  * Use with datatypes
  * Use with Hiera
  * Use with native functions
  * Use in defined types and classes


### Is Puppet Debugger a unit test replacement?
Puppet Debugger is not a replacement for proper unit testing.  Unit testing is used to catch errors across code changes while a debugger does not.  However, Puppet Debugger can be used to to evaluate complex code where you might have used a unit test before to dump output to STDOUT.  

### What does Puppet Debugger replace?
Using Puppet's traditional debugging methods is inefficient when compared to a live interactive console.  Puppet Debugger replaces these archaic methods:

* slow `puppet apply` examples
* notify resources
* dump_args, and echo functions
* other crafty ways to get variable values into STDOUT

Puppet Debugger will show you the before, during, and after state, while other tools show the before and after state only. 

### What makes Puppet Debugger unique?
* Pluggable expansion system, all debugger commands are plugins
* Works with multiple Puppet versions
* Community driven
* Emulates a real or fake node

### Where you can run Puppet Debugger?
Puppet Debugger was designed to run on all major platforms.  Only requirement is a modern ruby installation.

* Linux
* Mac
* Windows

### Which Puppet versions does the debugger support?

- Puppet 3 (deprecated, we stopped testing for Puppet 3 after version 0.10)
- Puppet 4 (tested with 4.10)
- Puppet 5 (test with 5.5.x)
- Puppet 6 (tested with 6.4)

### What kinds of things can a normal debugger do?
* Inspect variables (see values)
* Run code
* Set breakpoints
* evaluate code
* skip, break, retry, jump in, jump out of code blocks

### What can Puppet Debugger do?

* Inspect variables in various scopes
* View facts and their structures
* List and run functions in scope
* Playback code
* Run arbitrary code
* Create classes, functions, defined types, datatypes, plans, tasks
* Lookup data with Hiera lookup calls
* Mock other operating systems
* Set breakpoints

### What does Puppet Debugger not do?

* Cannot be used to enforce the catalog
* Should never run with Puppet agent catalog compilation
* skip, retry, jump in, jump out (currently not implemented)

### Where can Puppet Debugger be used?
ANY PUPPET CODE.  ANY.

* In combination with unit tests using the debug::break() function
* Used instead of `puppet apply` or even with `puppet apply` using the debug::break() function
* Puppet tasks and plans
* Puppet native functions
* Puppet classes and defined types
* Puppet data types


### Why is Puppet Debugger better than the normal `puppet apply`?

`Puppet apply` has been the go-to method for evaluating code. However, it is slow and requires the usage of notify resources or other functions to dump values to STDOUT.  Puppet Debugger offers:

* Faster results
* Interactive
* No need to write notify resources
* Can even use one-liners if you want


### Things Puppet Debugger needs internally to evaluate code?
When the Puppet compiler evaluates code, it requires the presence of the following things internally just like Puppet.  

* Node Object
* Facts Hash
* Scope Object
* Environment Object
* Modules

## Exercises

### 1. Installation
Puppet Debugger is Ruby gem that requires Puppet and Facter to be present.  You can use the PDK, Puppet-agent or custom Ruby installation.  It is better to use the PDK or Puppet-agent since it combines everything you need into a simple package. 

I have a provided some basic instructions for each type of installation. *Use only one of the methods below.*  If you want the path of least resistance, use the Docker method.

When complete, you should be able to run `puppet debugger` and see a similar prompt.

```
$ Puppet debugger
Ruby Version: 2.4.5
Puppet Version: 5.5.12
Puppet Debugger Version: 0.12.2
Created by: NWOps <corey@nwops.io>
Type "commands" for a list of debugger commands
or "help" to show the help screen.


1:>>
```

#### Docker method (easiest)
If you prefer containers you can start up a vanilla Ruby container or using an existing Puppet-agent container.  For a vanilla Ruby container, do the following:

```
docker run -ti ruby:2.4 /bin/bash
root@e98c7330ba7c:/# gem install puppet-debugger --no-rdoc --no-ri
root@e98c7330ba7c:/# puppet debugger

```

If you want the pre-built Puppet Debugger container image, then just run the following:

`docker run -ti nwops/puppet-debugger`

**NOTE**  This image already has the modules installed as well.  So there is nothing you need to install.  Don't you just love containers?

Dockerfile used to build this image:

```
FROM ruby:2.4
RUN gem install puppet-debugger --no-rdoc --no-ri
RUN puppet module install nwops-debug && \
    puppet module install puppet-nodejs && \
    puppet module install puppetlabs-ntp
CMD /bin/bash
```

#### Installing alongside the Puppet Agent's Ruby

Due to the following packaging bugs [PA-2670](https://tickets.puppetlabs.com/browse/PA-2670) and [FACT-1918](https://tickets.puppetlabs.com/browse/FACT-1918) installing on the Puppet agent requires renaming a few files. 

You can run the following script to auto-fix for both of these issues.

Failure to do this step will show an attempt to overwrite existing executables. 

`facter's executable "facter" conflicts with /opt/puppetlabs/puppet/bin/facter`

Run this script on a Puppet agent installation:

```shell
cd /opt/puppetlabs/puppet/lib/ruby/gems/*/specifications
mv hiera.gemspec hiera-$(hiera -v).gemspec
mv puppet.gemspec puppet-$(puppet --version).gemspec
if [[ $(facter kernel) == 'Darwin' ]]; then
  facter_version=$(facter --version | grep -Eo '\d\.\d{1,2}\.\d')
else
  facter_version=$(facter --version | grep -Po '\d\.\d{1,2}\.\d')
fi
sed -i s/2.1.7/2.1/ facter.gemspec
mv facter.gemspec facter-${facter_version}.gemspec
```

Once complete, run the gem command to install the debugger on your Puppet agent.

`sudo /opt/puppetlabs/puppet/bin/gem install puppet-debugger --no-ri --no-rdoc`

Or if you wish to install in user space:

`/opt/puppetlabs/puppet/bin/gem install puppet-debugger --no-ri --no-rdoc --user-install`

If you see this warning, make sure you add the specified path to your path variable.

```
WARNING:  You don't have /home/user_1/.gem/ruby/2.4.0/bin in your PATH,
	  gem executables will not run
export PATH=$PATH:/home/user_1/.gem/ruby/2.4.0/bin	  
```

#### Installing on the PDK

Installing on the PDK means having Puppet Debugger as a gem dependency. You can edit your .sync.yml file to add Puppet Debugger gem to your module's Gemfile.

```yaml
# .sync.yml
Gemfile:
  required:
    ":development":
      - gem: "puppet-debugger"
        version: "~> 0.12"

pdk update
bundle install
bundle exec puppet debugger
```

In order to use with the PDK, you will also have to run `bundle install`. 

**NOTE** If anyone knows a better way to use this tool with the PDK, please let me know.

#### Installing in user-supplied Ruby
First you will need Ruby >= 2.2 or whatever the Puppet version you have installed supports.  

Then it is just a matter of installing the gem (user space).

`gem install puppet-debugger --no-ri --no-rdoc --user-install`

If you see this warning, make sure you add the specified path to your path variable.

```
WARNING:  You don't have /home/user_1/.gem/ruby/2.4.0/bin in your PATH,
	  gem executables will not run
export PATH=$PATH:/home/user_1/.gem/ruby/2.4.0/bin	  
```

### 1b. Verify the installation works
Successful installation means you can run Puppet Debugger as a Puppet command.

```
$ puppet debugger
Ruby Version: 2.4.5
Puppet Version: 5.5.12
Puppet Debugger Version: 0.12.2
Created by: NWOps <corey@nwops.io>
Type "commands" for a list of debugger commands
or "help" to show the help screen.


1:>>
```

**NOTE** If you are using RVM, then `puppet debugger` may not work due to a "feature" in Puppet.  As a workaround, you can run `puppet-debugger` executable but this works differently than the Puppet application `puppet debugger` that is used in the examples below.  You may want to try the Docker method instead if that is available to you.

### 2. Installing Modules
The debugger uses the Puppet environment settings to find modules.  This is no different when using `puppet apply.`  By default, Puppet will load the modulepath defined in the configuration file or use the default value.  You can also override these settings with the `puppet debugger --modulepath=/opt/modules` directive. 

Run `puppet config print modulepath` or `puppet config print basemodulepath` to print your current default configs.

Installing new puppet modules is easy with the PMT.  

Please install the following modules:

```
puppet module install puppet-nodejs
puppet module install nwops-debug --version 0.1.3
puppet module install puppetlabs-ntp  # This must be last
```

Feel free to install other modules because they will show up in Puppet Debugger.


### 3. Running with a manifest
Puppet Debugger is nothing more than an interactive Puppet manifest. You can write anything you would normally write in a manifest.  You can even feed in a manifest file from STDIN, path to file or use the `-e` switch.   Puppet Debugger was designed to work similar to `puppet apply` in this regard. 

If a manifest or commands are fed into Puppet Debugger, the debugger tool will execute them and then prompt for additional commands.  Give each one of these examples a try.

Execute code and exit

* `echo "3 + 3" | puppet debugger --test`
* `puppet debugger --test -e "3 + 3"`

Execute code from a file

```shell
echo '3+3' > test.pp
puppet debugger -p test.pp
```

Execute code from a url

```shell
puppet debugger -p https://gist.github.com/logicminds/4f6bcfd723c92aad1f01f6a800319fa4
```

#### Sharable links
You may have seen me (nwops) paste code snippets in the Puppet community Slack channel that references a url.  You can do this yourself with a simple gist or url hosted on the internet via the http play resource and url parameter.   

https://demo.puppet-debugger.com/play?url=https://gist.github.com/logicminds/4f6bcfd723c92aad1f01f6a800319fa4

Or inline a small code snippet with the content parameter.

https://demo.puppet-debugger.com/play?content=md5('dafsdfdsfasdfds')

The demo site is a web application that runs a local instance of the debugger per session.  

So create a link, test it and share it.

### 4. Listing and executing functions
Puppet Debugger was designed to help you determine what a function returns in real time as well as what functions are available from each module.

The debugger will search your module path for functions and present you with a nice list of functions for you to explorer and execute.

The debugger outputs a table that displays the name and the module from which it was found.  Functions found in Puppet core are labeled with the version of Puppet currently running.

To list the functions just use the command `functions`

```
1:>> functions
FULL_NAME                  | MOD_NAME
---------------------------|-------------
abs                          | puppet-5.5.12
alert                         | puppet-5.5.12
all                            | puppet-5.5.12
annotate                  | puppet-5.5.12
any                          | puppet-5.5.12
any2array                | stdlib
any2bool                 | stdlib
assert_private         | stdlib
```

To run a function, you just type the name of the function and supply the arguments.

```
4:>> md5('hello').upcase
 => "5D41402ABC4B2A76B9719D911017C592"
5:>>
```

**TIP** Use TAB to auto-complete the function name. 

### 5. Listing resources and evaluated parameters
A resource is the result of the evaluated type that was compiled into the catalog.  You can get a list of all the resources including the parameters that were in that resource. 

To list a resource, you first must include a resource from an installed module.

```
  5:>> include nodejs
  => Puppet::Type::Component {
  loglevel => notice,
      name => "Nodejs",
     title => "Class[Nodejs]"
}

```

OR

```
  5:>> class{'nodejs': }
  => Puppet::Type::Component {
  loglevel => notice,
      name => "Nodejs",
     title => "Class[Nodejs]"
}

```

List all the resources in the current catalog.  This shows you all the resources that were included as a result of 

```
6:>> resources
Resources not shown in any specific order
[
  [ 7] "Class['Nodejs::Params']",
  [ 8] "Class['Nodejs']",
  [ 9] "Class['Nodejs::Install']",
  [10] "Package['nodejs']",
  [11] "Package['nodejs-devel']",
  [12] "Package['nodejs-debuginfo']",
  [13] "Package['npm']",
  [14] "File['root_npmrc']",
  [15] "Class['Nodejs::Repo::Nodesource']",
  [16] "Class['Nodejs::Repo::Nodesource::Yum']",
  [17] "Yumrepo['nodesource']",
  [18] "Yumrepo['nodesource-source']",
  [19] "File['/etc/pki/rpm-gpg/NODESOURCE-GPG-SIGNING-KEY-EL']"
]
```
For additional debugging you can get a list of all the parameters of each resource.  This means listing the variables in the resource as they were evaluated.  Any hiera lookups performed or variable interpolation will be represented in the output.
The argument to `ls` is just a filter.  So if you supply a type or a resource title you will get different results.

```
6:>> ls nodejs
{
 "Class[Nodejs::Install]"               => {},
 "Class[Nodejs::Params]"                => {},
 "Class[Nodejs::Repo::Nodesource::Yum]" => {},
 "Class[Nodejs::Repo::Nodesource]"      => {},
 "Class[Nodejs]"                        => {
  "cmd_exe_path"                => nil,
  "manage_package_repo"         => true,
  "nodejs_debug_package_ensure" => "absent",
  "nodejs_debug_package_name"   => "nodejs-debuginfo",
  "nodejs_dev_package_ensure"   => "absent",
  "nodejs_dev_package_name"     => "nodejs-devel",
  "nodejs_package_ensure"       => "present",
  "nodejs_package_name"         => "nodejs",
  "npm_package_ensure"          => "absent",
  "npm_package_name"            => "npm",
  "npm_path"                    => "/usr/bin/npm",
  "npmrc_auth"                  => nil,
  "npmrc_config"                => nil,
  "package_provider"            => nil,
  "repo_class"                  => "::nodejs::repo::nodesource",
  "repo_enable_src"             => false,
  "repo_ensure"                 => "present",
  "repo_pin"                    => nil,
  "repo_priority"               => "absent",
  "repo_proxy"                  => "absent",
  "repo_proxy_password"         => "absent",
  "repo_proxy_username"         => "absent",
  "repo_release"                => nil,
  "repo_url_suffix"             => "8.x",
  "use_flags"                   => [
   [0] "npm",
   [1] "snapshot"
  ]
 },
 "Package[nodejs-debuginfo]"            => {
  "ensure"   => "absent",
  "provider" => nil,
  "tag"      => "nodesource_repo"
 },
 "Package[nodejs-devel]"                => {
  "ensure"   => "absent",
  "provider" => nil,
  "tag"      => "nodesource_repo"
 },
 "Package[nodejs]"                      => {
  "ensure"   => "present",
  "provider" => nil,
  "tag"      => "nodesource_repo"
 }
}
```

### 6. Listing facts and variables
In addition to listing the variables of an evaluated resource, you can get the current scope and list all the variables currently set.

The scope used during Puppet compilation can be inspected at any time.  

Use the `vars` or `ls` command to show the contents of the current scope.

```
8:>> ls
"Facts were removed for easier viewing"
{
 "datacenter"   => "datacenter1",
 "facts"        => "removed by the puppet-debugger",
 "module_name"  => "",
 "name"         => "main",
 "server_facts" => {
  "environment"   => "production",
  "serverip"      => "10.0.3.154",
  "servername"    => "MacBook-Pro-162",
  "serverversion" => "6.4.0"
 },
 "title"        => "main",
 "trusted"      => {
  "authenticated" => "local",
  "certname"      => nil,
  "domain"        => nil,
  "extensions"    => {},
  "hostname"      => nil
 }
}
```

To list the facts in the scope simply run the `facts` command or `$::facts`

```
6:>> facts
{
 "architecture"              => "x86_64",
 "augeas"                    => {
  "version" => "1.4.0"
 },
 "augeasversion"             => "1.4.0",
 "bios_release_date"         => "12/01/2006",
 "bios_vendor"               => "innotek GmbH",
 }
 
6:>> $facts
{
 "architecture"              => "x86_64",
 "augeas"                    => {
  "version" => "1.4.0"
 },
 "augeasversion"             => "1.4.0",
 "bios_release_date"         => "12/01/2006",
 "bios_vendor"               => "innotek GmbH",
 }

```

Show any variable simply by typing the name of the variable just like you would in a manifest.

```
7:>> $os
 => {
  "architecture" => "x86_64",
        "family" => "RedHat",
      "hardware" => "x86_64",
          "name" => "Fedora",
       "release" => {
     "full" => "23",
    "major" => "23"
  },
       "selinux" => {
       "config_mode" => "permissive",
     "config_policy" => "targeted",
      "current_mode" => "permissive",
           "enabled" => true,
          "enforced" => false,
    "policy_version" => "29"
  }
}
```

**TIP** Use tab to auto complete the variable name. 

### 7. Prototyping datatypes
You can easily test new and existing datatypes in realtime using the debugger.  Use the examples below to test and create your own datatype.
 
#### Datatype comparisons
The puppet language has a special operator `=~` that allows you to compare the type of variable for equality.   Try each of the following commands yourself to get a feel for how this works.  


```
8:>> $os =~ Hash
 => true
9:>> $os['family']
 => "RedHat"
10:>> $os['family'] =~ String
 => true
11:>> $os['family'] =~ Boolean
 => false
12:>> $os['family'] =~ Enum['redhat']
 => false
13:>> $os['family'] =~ Enum['RedHat']
 => true
14:>>
```

You can also utilize the `type_of` function to report what datatype a variable is.  Although this may result in ambiguous data types.


```
2:>> md5('hello').upcase.type_of
 => String

```

#### Datatype composition
You can create datatypes right inside the debugger by using the `type` keyword.   

```
15:>> type RedhatOSType = Enum['RedHat']
16:>> $os['family'] =~ RedhatOSType
 => true
17:>>
```

While this is a simple example think about something more complex.  Take a look at this [example](https://gitlab.com/blockops/puppet-crossbelt/blob/master/types/authorizedkeys.pp) and
[example2](https://gitlab.com/blockops/puppet-crossbelt/blob/master/types/authorizedkey.pp)

When datatypes get complex we often only check the basics because we don't understand them fully.  But having a way to easily validate your validation allows you to create a more restrictive datatype. 

### 8. Hiera debugging
You can use the debugger to help you figure out how hiera is resolving the keys in addition to be able to play with the data that it returns.  Hiera lookups are nothing more than a call to a function.  In fact the name of the function is called `lookup`.

Before we get into this exercise you will need to download the following example I have setup.  Please run the following commands:

```
git clone https://gitlab.com/puppet-debugger/puppet-debugger-hiera-examples.git
cd puppet-debugger-hiera-examples
mkdir -p $(puppet config print confdir)
puppet config set hiera_config ${PWD}/hiera.yaml

```

Once setup you can then use the debugger to lookup values that reference the data just cloned.

```
1:>> lookup('complex_type1')
 => [
  [0] {
    "name" => "value1"
  },
  [1] {
    "name" => "value2"
  },
  [2] {
    "name" => "value3"
  }
]
2:>>
```

**NOTE** Because the debugger is a long running process, hiera will cache the values.  So if you change the value when the debugger is running hiera will not use the new value right away until the cache is reset.  You can use the debugger's `reset` command to reset the entire environment or restart the debugger.


### 9. Turning on puppet debug messages.
In order to turn on extra logging, just use the builtin puppet switch.  `puppet debugger --log-level=debug`

This will result in extra messages being dumped into the console.

Additionally, you can toggle this output within the debugger using.

`set loglevel debug` or `set loglevel info` to remove debug mesages.

This is extremely useful when debugging hiera lookups as the hiera explain output is also displayed.

So give this a try when using lookups or other things.

```
set loglevel debug
lookup('complex_type1')
```

### 10. Mocking custom facts or using prebuilt facts
The debugger uses [facterdb](https://github.com/camptocamp/facterdb) by default to get a pre-canned set of facts.  This is partly why it starts up so fast.  You can tell the debugger to use a different set of facts from a different kernel, OS version, facter version or any custom combination that facterdb supports.  This is all controlled via the facterdb-filter.

Below are some examples of loading up different fact sets for various operating systems and then running a lookup for each example fact set.  Give each one of these a try.

```
puppet debugger --facterdb-filter 'facterversion=/^3.6./ and operatingsystem=windows' --test -e '$os[family]'

puppet debugger --facterdb-filter 'facterversion=/^3.6./ and osfamily=RedHat' --test -e '$os[family]'

puppet debugger --facterdb-filter 'facterversion=/^3.6./ and osfamily=Darwin' --test -e '$os[family]'

```

You can force the debugger to use real facts from the current node too:

``puppet debugger --no-facterdb --test -e '$os[family]'``

### 11. Using a breakpoint
A breakpoint allows us to stop the code compilation process in real time and inspect
all the variables in scope.  The debugger uses a function called `debug::break()` to perform this magic.  Since the `debug::break()` is a puppet function you can use it anywhere in puppet code.  One of the best places is using inside a looping construct
such as reduce.   

Use the example below to sum all the numbers together into a new variable.   The debugger will run and then stop to allow you to inspect the ephemeral scope variables. Take a look at $acc and $value with each iteration when doing this.  Type `exit` to exit to the next iteration.

```
1:>> [1,2,3].reduce | $acc, $value | {
  2:>>   debug::break()
  3:>>   $acc + $value
  4:>> }
From file: puppet_debugger_input20190601-145-9kqyur.pp
         1: [1,2,3].reduce | $acc, $value | {
      => 2:   debug::break()
         3:   $acc + $value
         4: }
1:>> $acc
 => 1
2:>> $value
 => 2
3:>> exit
From file: puppet_debugger_input20190601-145-9kqyur.pp
         1: [1,2,3].reduce | $acc, $value | {
      => 2:   debug::break()
         3:   $acc + $value
         4: }
1:>> $acc
 => 3
2:>> $value
 => 3
3:>> exit
 => 6
```

**TIP** Anytime you use a debug::break() function inside an iteration block you can type `exit` to exit the current iteration.

**NOTE** If you received `Evaluation Error: Unknown function: 'debug::break'. (line: 3, column: 3)` you must ensure you have the debug module installed.  `puppet module install nwops-debug --version 0.1.3`


### 12. Breaking into your code
All the examples above have used the debugger as the starting point to run commands and evaluate code.  But what if you wanted to start the debugger during a unit test or during `puppet apply`?  This is where the `debug::break()` function comes into play.

You can use the `debug::break()` function to insert a breakpoint anywhere in your puppet code and puppet will run the break function which results in an interactive debugger session loaded with the current environment, scope and node object.  This allows you to evaluate and inspect your code on the fly.  

You can read about this in detail [here](http://logicminds.github.io/blog/2017-04-25-break-into-your-puppet-code/).

**NOTE** Remember, because you started from a unit test or `puppet apply` you will get real facts this time or ones provided by rspec-puppet.  

The easiest way to see this in action is to perform the following steps:

```
cd ~
git clone https://gitlab.com/puppet-debugger/debugger_demo_module.git
cd debugger_demo_module
bundle install --jobs 4 # ( coffee break )
bundle exec rake spec
```

Which should look something like:

```
 bundle exec rake spec
 
demo
  on windows-2008 R2-x64
Ruby Version: 2.5.1
Puppet Version: 6.4.2
Puppet Debugger Version: 0.12.3
Created by: NWOps <corey@nwops.io>
Type "commands" for a list of debugger commands
or "help" to show the help screen.


From file: init.pp
          7: class demo(
          8:   Array[Integer] $array = [1,2,3]
          9: ) {
         10:
         11:   $array.each | $value | {
      => 12:     debug::break()
         13:     file{"/tmp/test${value}.txt": ensure => present }
         14:   }
         15:
         16: }
```

Take a moment to play around inside the code.  Try to determine which OS is currently being tested.  Use the `ls` command, `resources` command.  Look at the `$::facts[os]` variable to see which OS rspec-puppet is currently using.

Execute the `"/tmp/test${value}.txt"` in the debugger to see the result of the variable interpolation. 

**TIP** Anytime you use a debug::break() function inside an iteration block you can type `exit` to exit the current iteration.

**TIP** This may iterate many times, use control-c to close the debugger if you don't want to cycle through all the iterations.

#### Play Time
If you are reading this section, then you have probably completed all the exercises and have a more in-depth understanding with the debugger.  Congratulations! Spend some time with Puppet Debugger using your own code snippets.  Walk through some complex code blocks and get a better understanding of how the language works. 

### Summary
This workshop was just an introduction into what Puppet Debugger provides. There are MANY other possibilities that you will discover as you continue using this tool.  

So try to integrate Puppet Debugger into your normal development workflow.  You will find yourself answering questions without the help of others in far less time.

## How can you help?
As Puppet Debugger is a community-driven tool, its success depends upon the community which uses it.  Your time is the most valuable resource you can provide.  The easiest thing you can do is spread the word.  Tell people.  Show people.  Help them so they can help others and themselves.  Post about Puppet Debugger, chat it up on Reddit or wherever you get your news.  Saving someone 10 minutes from their daily routine can be life-changing for them. 

#### Documentation
Puppet Debugger is undergoing a major documentation update.  Any demos, videos, blogs, section explanations, or use cases are greatly appreciated.  Please create an issue or PR on github.com/nwops/puppet-debugger

#### Code cleanup
I started writing this tool a few years ago.  My Ruby skills have improved, but I have not reflected my newly-gained knowledge in the code everywhere.  Feel free to submit a PR with a code enhancement.

#### New plugins
Puppet Debugger's plugin system allows you to extend the reach of the debugger with new capabilities.  See the [plugin doc](https://github.com/nwops/puppet-debugger/blob/master/Plugin_development.md) if you wish to create something.  Any new plugin you create can be distributed as a separate gem. If you do create a plugin, please let me know so I can add it to the list of plugins.

