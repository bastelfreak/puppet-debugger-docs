+++
title = "Commands"
date = 2019-05-21T16:12:08-07:00
weight = 30
#chapter = true
#pre = "<b>X. </b>"
+++

The debugger comes with many commands that allow you to inspect the catalog, your code and reference items currently in scope.

If you forget which commands are available, use the `commands` command.

```shell
1:>> commands
 Context
   environment          Show the current environment name
   reset                Reset the debugger to a clean state.
   whereami             Show code surrounding the current context.

 Editing
   play                 Playback a file or URL as input.

 Environment
   datatypes            List all the datatypes available in the environment.
   functions            List all the functions available in the environment.
   types                List all the types available in the environment.

 Help
   commands             List all available commands, aka. this screen
   exit                 Quit Puppet Debugger.
   help                 Show the help screen with version information.

 Node
   classification       Show the classification details of the node.
   facterdb_filter      Set the facterdb filter
   facts                List all the facts associated with the node.

 Scope
   classes              List all the classes current in the catalog.
   krt                  List all the known resource types.
   resources            List all the resources current in the catalog.
   set                  Set the a puppet debugger config
   vars                 List all the variables in the current scopes.

 Tools
   benchmark            Benchmark your Puppet code.

```

If you forgot which command shows the commands, use the `help` command.

```shell
1:>> help
Ruby Version: 2.5.1
Puppet Version: 6.4.0
Puppet Debugger Version: 0.12.0
Created by: NWOps <corey@nwops.io>
Type "commands" for a list of debugger commands
or "help" to show the help screen.


1:>>
```
