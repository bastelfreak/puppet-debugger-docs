+++
title = "Resources"
date = 2019-05-21T16:12:55-07:00
weight = 5
chapter = false
#pre = "<b>X. </b>"
+++

### Summary

List all the resources currently in the catalog. Remember the puppet debugger is like an interactive manifest file. When you type puppet code, puppet will evaulate it and add it to the catalog.

```shell
1:>> include nodejs
2:>> resources
Resources not shown in any specific order
[
  [ 0] "Stage['main']",
  [ 1] "Class['Settings']",
  [ 2] "Class['main']",
  [ 3] "Node['__node_regexp__foo']",
  [ 4] "Notify['found foo node']",
  [ 5] "Class['Nodejs::Params']",
  [ 6] "Class['Nodejs']",
  [ 7] "Class['Nodejs::Install']",
  [ 8] "Package['nodejs']",
  [ 9] "Package['nodejs-devel']",
  [10] "Package['nodejs-debuginfo']",
  [11] "Package['npm']",
  [12] "File['root_npmrc']",
  [13] "Class['Nodejs::Repo::Nodesource']",
  [14] "Class['Nodejs::Repo::Nodesource::Yum']",
  [15] "Yumrepo['nodesource']",
  [16] "Yumrepo['nodesource-source']",
  [17] "File['/etc/pki/rpm-gpg/NODESOURCE-GPG-SIGNING-KEY-EL']"
]
```
