+++
title = "Introduction"
date = 2019-05-21T15:50:22-07:00
weight = 1
chapter = true
#pre = "<b>1. </b>"
+++

# Introduction

The debugger is a puppet application packaged as a ruby gem that is installed along side puppet.

The primary purpose is to allow you to debug your existing code logic and allow you to see how your puppet code is compilied.

If you have ever used `puppet apply` to evaulate simple puppet code snippets, this replaces that entire workflow with a simple interactive console.

Since the beginning of puppet there has never been a simple way to run arbitrary puppet code without affecting a real system. We have always used simple variable dump hacks like puppet apply and a notify resource to spit out the result of some code.

Almost all programming languages provide a debugging console that let you play around with code interactively. Except Puppet.

- irb
- python
- go playground
- js console

Thus the debugger was born.

The debugger parses and evaluates your code and then allows you to inspect the compilation process, the resultant catalog in ways the puppet apply command never allowed or you never imagined. Unlike puppet apply, the debugger never enforces the catalog, so nothing will ever be modified.
