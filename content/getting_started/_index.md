+++
title = "Getting Started"
date = 2019-05-21T14:22:53-07:00
weight = 5
#chapter = false
#pre = "<b>X. </b>"
+++

### Requirements

- Ruby 2.1+

{{% notice note %}}
While Ruby 2.1 will work it is recommended you run a newer, supported version of ruby.
{{% /notice %}}

#### Supported Operating Systems

- OS X
- Linux
- Windows

#### Supported Puppet Runtimes

- Puppet 4
- Puppet 5
- Puppet 6

**Note** Puppet 3 may work but since it is EOL you may run into issues.

### Installation

#### On a development system (Recommended)

To get started install the gem on your development system.

`gem install puppet-debugger` (With admin access)

`gem install puppet-debugger --user-install` (without admin access)

Don't have root? `

#### Installing on a puppet agent

Due to the following packaging bugs PA-2670 and FACT-1918 installing on the puppet agent requires renaming a few files. You can run the following script to
auto fix.

```shell
cd /opt/puppetlabs/puppet/lib/ruby/gems/2.5.0/specifications
mv hiera.gemspec hiera-$(hiera -v).gemspec
mv puppet.gemspec puppet-$(puppet --version).gemspec
mv facter.gemspec facter-$(facter -v | sed )
```

Once complete run the gem command to install the debugger on your puppet agent.

`/opt/puppetlabs/puppet/bin/gem install puppet-debugger --no-ri --no-rdoc`

#### Installing on the PDK

Installing on the pdk means having the puppet-debugger as a dependency. You can edit your .sync.yml file to add the puppet-debugger gem to your module.

```yaml
# .sync.yml
Gemfile:
  Required:
    puppet-debugger: true

pdk update
```

#### In the Gemfile

Not using the PDK? You can add to your Gemfile.

`gem 'puppet-debugger'`

#### On the puppetserver

**Stop! Don't do this.**

This is a development tool and may cause issues with your puppetserver.

### Usage

The puppet debugger is a puppet application so once you install the gem, just fire it up using `puppet debugger`.

```shell
$ puppet debugger
Ruby Version: 2.5.1
Puppet Version: 6.4.0
Puppet Debugger Version: 0.12.0
Created by: NWOps <corey@nwops.io>
Type "commands" for a list of debugger commands
or "help" to show the help screen.


1:>>
```
